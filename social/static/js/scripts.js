$(document).ready(function(){

    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
               xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    $('a.like').click(function(e){
        e.preventDefault();
        $.post('{% url "images:like" %}',
        {id: $(this).data('id'),
        action: $(this).data('action')},
        // CALLBACK FUNCTION (при ответе сервера)
        function(data){
            if (data['status'] == 'ok') {
                var previous_action = $('a.like').data('action');
                // toggle data-action
                $('a.like').data('action', previous_action == 'like' ? 'unlike' : 'like');
                // toggle link text
                $('a.like').text(previous_action == 'like' ? 'Unlike' : 'Like');
                // update total likes
                var previous_likes = parseInt($('#total').text());
                $('#total').text(previous_action == 'like' ? previous_likes + 1 : previous_likes - 1);
            }
        }
      );
    });
});