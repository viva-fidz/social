from django.urls import path, re_path
from .views import image_create, image_detail, image_like


app_name = 'images'

urlpatterns = [
    path('create/', image_create, name='create'),
    re_path('detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$', image_detail, name='detail'),
    path('like/', image_like, name='like'),
]
