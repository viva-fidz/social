from django.urls import path, re_path
from django.contrib.auth.views import logout, login, logout_then_login, password_change, password_change_done, \
    password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from .views import dashboard, register, edit

app_name = 'account'

urlpatterns = [
    # url(r'^login/$', views.user_login, name='login'),
    path('dashboard', dashboard, name='dashboard'),

    # LOGIN urls
    re_path('^login/$', login, name='login'),  # по дефолту ищет в registration/login.html
    path('logout', logout, name='logout'),
    path('logout-then-login', logout_then_login, name='logout_then_login'),

    # PASSWORD urls
    path('password-change/done', password_change_done, name='password_change_done'),
    path('password-change', password_change, name='password_change'),
    path('password-reset/done/', password_reset_done, name='password_reset_done'),
    path('password-reset/complete/', password_reset_complete, name='password_reset_complete'),
    path('password-reset/', password_reset, name='password_reset'),
    re_path('password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/', password_reset_confirm,
            name='password_reset_confirm'),

    # REGISTER urls
    path('register/', register, name='register'),

    # EDIT urls
    path('edit/', edit, name='edit'),
    re_path('^', dashboard, name='dashboard'),

]
