from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords
from .models import Product


class LatestProductsFeed(Feed):
     # <title>, <link>, and <description>  RSS elements
    title = 'MyShop Feed'
    link = '/shop/'
    description = 'New products in MyShop.'


    def items(self):
        prod = Product.objects.all()
        return prod.order_by('-updated')[:3]

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return truncatewords(item.description, 20)