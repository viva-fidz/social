from django import template
from django.db.models import Count
from django.utils.safestring import mark_safe
import markdown


register = template.Library()

from ..models import Product


@register.simple_tag   #  returns string
def total_products():
    return Product.objects.filter(available=True).count()


@register.inclusion_tag('shop/product/latest_products.html')   # returns a rendered template (dict)
def latest_products(count=2):
    products = Product.objects.filter(available=True)
    latest_products = products.order_by('-updated')[:count]
    return {'latest_products': latest_products}


@register.simple_tag  # assignment_tag  sets a variable in the context
def get_most_commented_products(count=2):
    return Product.objects.filter(available=True).annotate(total_comments=Count('comments')).order_by('-total_comments')[:count]



@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text))