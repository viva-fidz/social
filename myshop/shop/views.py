from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.db.models import Count
from django.views.generic import ListView
from .forms import EmailPostForm, CommentForm
from .models import Category, Product, Comment
from taggit.models import Tag


def product_list(request, category_slug=None, tag_slug=None):
    category = None
    tag = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        products = products.filter(tags__in=[tag])
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    paginator = Paginator(products, 2)
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    return render(request, 'shop/product/list.html', {'category': category,
                                                      'categories': categories,
                                                      'products': products,
                                                      'page': page,
                                                      'tag': tag
                                                      })


class ProductListView(ListView):
    queryset = Product.objects.all()
    context_object_name = 'products'
    paginate_by = 2
    template_name = 'shop/product/list.html'


def product_detail(request, id, slug):
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    comments = product.comments.filter(active=True)
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.product = product
            new_comment.save()
    else:
        comment_form = CommentForm()
    # List of similar products
    product_tags_ids = product.tags.values_list('id', flat=True)
    similar_products = Product.objects.filter(tags__in=product_tags_ids).exclude(id=product.id)
    similar_products = similar_products.annotate(same_tags=Count('tags')).order_by('-same_tags')[:4]
    return render(request, 'shop/product/detail.html', {'product': product,
                                                        'comments': comments,
                                                        'comment_form': comment_form,
                                                        'similar_products': similar_products})


def product_share(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    sent = False
    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            product_url = request.build_absolute_uri(product.get_absolute_url())
            subject = '{} ({}) recommends you reading "{}"'.format(cd['name'], cd['email'], product.name)
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(product.name, product_url, cd['name'],
                                                                     cd['comments'])
            send_mail(subject, message, 'crowd.scoring@yandex.ru', [cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    return render(request, 'shop/product/share.html', {'product': product, 'form': form, 'sent': sent})
